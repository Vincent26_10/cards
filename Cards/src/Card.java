
public class Card {

	private int number;
	private Suit suit;

	public Card(int number, Suit suit) {
		this.number = number;
		this.suit = suit;
	}

	@Override
	public String toString() {
		String s = "";

		switch (number) {
		case 1:
			s += "as";
			break;

		case 10:
			s += "sota";
			break;
		case 11:
			s += "caballo";
			break;
		case 12:
			s += "rey";
			break;

		default:
			s += "" + number;
			break;
		}

		switch (suit) {
		case OROS:
			s += "oros";
			break;
		case BASTOS:
			s += "bastos";
			break;

		case ESPADAS:
			s += "espadas";
			break;

		case COPAS:
			s += "copas";
			break;

		}

		return s;
	}

}
