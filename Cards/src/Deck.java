
public class Deck {
	private Card[] deck;
	private int number;

	public Deck() {
		deck = new Card[48];
		number =47;
		for (int i = 0; i < 12; i++) {
			deck[i] = new Card(i+1, Suit.BASTOS);
			deck[i + 12] = new Card(i+1, Suit.COPAS);
			deck[i + 24] = new Card(i+1, Suit.ESPADAS);
			deck[i + 36] = new Card(i+1, Suit.OROS);
		}

	}

	public void shuffle() {
		Card c;
		for (int i = 0; i < 1000; i++) {
			int x = (int) (Math.random() * (number+1));
			int y = (int) (Math.random() * (number+1));
			c = deck[y];
			deck[y] = deck[x];
			deck[x] = c;
		}
	}
	
	public Card extractCard() {
		Card c = deck[number];
		number--;
		return c;		
	}
	
	public void print() {
        for (int i = 0; i < deck.length; i++) {
            System.out.println(deck[i]);
        }
	}
}
